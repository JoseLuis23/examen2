const calcular = document.getElementById('calcular');
calcular.addEventListener("click", function() {
    const cantidad = parseFloat(document.getElementById("cantidad").value);
    const grados = document.querySelector('input[name="grado"]:checked').value;
    let res = 0;

    if (grados === "cel") {
        res = (cantidad * 9/5) + 32;
    } else if (grados === "far") {
        res = (cantidad - 32) * 5/9;
    }

    document.getElementById("res").value = res.toFixed(2) + " °F";
});

const limpiar = document.getElementById('limpiar');
limpiar.addEventListener('click', function () {
    document.getElementById("cantidad").value = "";
    document.getElementById("res").value = "";
});

