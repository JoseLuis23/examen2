document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("btnGenerar").addEventListener("click", function () {
        generarnumeros();
    });

    document.getElementById("btnLimpiar").addEventListener("click", function () {
        Limpiar();
    });
});

function generarnumeros() {
    var txtEdades = document.getElementById("cantidad");
    var numeros = [];
    for (var i = 0; i < 100; i++) {
        var numerosRandom = Math.floor(Math.random() * 100) + 1;
        numeros.push(numerosRandom);
    }
    var numerosTexto = numeros.join(", ");
    txtEdades.value = numerosTexto;

    var be = 0;
    var niño = 0;
    var adolecent = 0;
    var adult = 0;
    var ancian = 0;

    for (var i = 0; i < numeros.length; i++) {
        var edad = numeros[i];
        if (edad >= 1 && edad <= 3) {
            be++;
        } else if (edad >= 4 && edad <= 12) {
            niño++;
        } else if (edad >= 13 && edad <= 17) {
            adolecent++;
        } else if (edad >= 18 && edad <= 60) {
            adult++;
        } else {
            ancian++;
        }
    }

    var sumaEdades = 0;
    for (var i = 0; i < numeros.length; i++) {
        sumaEdades += numeros[i];
    }
    var edadPromedio = sumaEdades / numeros.length;

    document.getElementById("bebe").value = be;
    document.getElementById("niño").value = niño;
    document.getElementById("adolescentes").value = adolecent;
    document.getElementById("adultos").value = adult;
    document.getElementById("ancianos").value = ancian;
    document.getElementById("prom").value = edadPromedio.toFixed(2);
}

function Limpiar() {
    document.getElementById("cantidad").value = "";
    document.getElementById("bebe").textContent = "0";
    document.getElementById("niño").textContent = "0";
    document.getElementById("adolescentes").textContent = "0";
    document.getElementById("adultos").textContent = "0";
    document.getElementById("ancianos").textContent = "0";
}
